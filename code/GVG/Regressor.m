classdef Regressor < handle
    properties
        X
        Xconj
        Xenv
        score
        reg
    end
    %%
    methods
    %%  Builder, Inputs:
    %      X = [m0,m1,m2,m3...]
    %      Xconj = [m0,m1,m2,m3...]
    %      Xenv = [m0,m1,m2,m3...]
        function self = Regressor(X, Xconj, Xenv)
            self.X = X;
            self.Xconj = Xconj;
            self.Xenv = Xenv;
            self.score = 0;
            self.reg = [];
        end
    %%  equal: Checkes if self and another reggresor object are the same
    %       They will be equal if their properties X,Xconj y Xenv are equal
        function equal = equals(self,H2)
%             fprintf('%s',self.X);
%             fprintf('%s',H2.X);
            equal = false;
            if (isequal(self.X,H2.X) || (isempty(self.X) && isempty(H2.X))) && (isequal(self.Xconj,H2.Xconj) || (isempty(self.Xconj) && isempty(H2.Xconj))) && (isequal(self.Xenv,H2.Xenv) || (isempty(self.Xenv) && isempty(H2.Xenv)))
                equal = true;
            end
        end
    %%  print: Shows regressors in format string
        function S = print(self)
            S = [];
            for i=1:length(self.X)
                S = sprintf('%s x(n-%d)',S,self.X(i));
            end
            for i=1:length(self.Xconj)
                S = sprintf('%s x*(n-%d)',S,self.Xconj(i));
            end
            for i=1:length(self.Xenv)
                S = sprintf('%s |x(n-%d)|',S,self.Xenv(i));
            end
        end
    %%  DEPRECATED, print_short:
    %       Use print_mig
        function S = print_short(self)
            S = [];
            Sabs = [];
            maxMemory = max([self.X,self.Xconj,self.Xenv]);
            for im = 0:maxMemory
                exponent = 0;
                while sum(self.X==im) > 0 && sum(self.Xconj==im) > 0
                    i1 = find(self.X==im,1,'first');
                    i2 = find(self.Xconj==im,1,'first');
                    self.X(i1)=[]; self.Xconj(i2)=[];
                    exponent = exponent + 2;
                end
                while sum(self.Xenv==im) > 1
                    i1 = find(self.Xenv==im,2,'first');
                    self.Xenv(i1)=[];
                    exponent = exponent + 2;
                end
                if sum(self.Xenv==im) == 1
                    i1 = find(self.Xenv==im,1,'first');
                    self.Xenv(i1)=[];
                    exponent = exponent + 1;
                end
                if exponent~=0
                    if im~=0
                    Sabs = sprintf('%s |x(n%+d)|^%d',S,-im,exponent);
                    else
                        Sabs = sprintf('%s |x(n)|^%d',S,exponent);
                    end
                end
            end
            
            for i=1:length(self.X)
                if(self.X(i)~=0)
                    
                    S = sprintf('%s x(n%+d)',S,-self.X(i));
                else
                    S = sprintf('%s x(n)',S);
                end
            end
            for i=1:length(self.Xconj)
                if(self.Xconj(i)~=0)
                    
                    S = sprintf('%s x*(n%+d)',S,-self.Xconj(i));
                else
                    S = sprintf('%s x*(n)',S);
                end
            end
            S = [S Sabs];
            for i=1:length(self.Xenv)
                if(self.Xenv(i)~=0)
                    S = sprintf('%s |x(n%+d)|',S,-self.Xenv(i));
                else
                    S = sprintf('%s |x(n)|',S);
                end
            end
        end
    %%  print_mig: Shows regressors in a more compact way
    %       Gruops consituents in exponents.
        function S = print_mig(self)
            S = [];
            self.order();
            [uX,~,iX] = unique(self.X);
            if ~isempty(iX)
                for i=uX
                    S = sprintf('%s x(n-%d)^%d',S,i,sum(i==self.X));
                end
            end
            [uXconj,~,iXconj] = unique(self.Xconj);
            if ~isempty(iXconj)
                for i=uXconj
                    S = sprintf('%s x*(n-%d)^%d',S,i,sum(i==self.Xconj));
                end
            end
            [uXenv,~,iXenv] = unique(self.Xenv);
            if ~isempty(iXenv)
                for i=uXenv
                    S = sprintf('%s |x(n-%d)|^%d',S,i,sum(i==self.Xenv));
                end
            end
        end
    %%  mutatememory: 
        function newreg = mutatememory(self)
            newreg = Regressor(self.X,self.Xconj,self.Xenv);
            ind = randi([1 length(self.X)+length(self.Xconj)+length(self.Xenv)]);
            if ind <= length(self.X)
                newreg.X(ind) = newreg.X(ind) + 1;
            elseif ind <= length(self.X)+length(self.Xconj)
                ind = ind-length(self.X);
                newreg.Xconj(ind) = newreg.Xconj(ind) + 1;
            else
                ind = ind - length(self.X) - length(self.Xconj);
                newreg.Xenv(ind) = newreg.Xenv(ind) + 1;
            end
        end
    %%  DEPRECATED, mutateorder: 
%         function self = mutateorder(self, P)
%             if rand<P
%                 if ~isempty(self.X)
%                     ind = randi([1 length(self.X)]);
%                     self.Xconj = [self.Xconj self.X(ind)];
%                     self.X(ind) = [];
%                 end
%             else
%                 if ~isempty(self.Xconj)
%                     ind = randi([1 length(self.Xconj)]);
%                     self.X = [self.X self.Xconj(ind)];
%                     self.Xconj(ind) = [];
%                 end
%             end
%         end
    %% mutateorderv2:
        function newreg = mutateorderv2(self)
            newreg = Regressor(self.X,self.Xconj,self.Xenv);
            ind = randi([1 length(self.X)+length(self.Xconj)+length(self.Xenv)]);
            if ind <= length(self.X)
                selected = randi([1,length(self.X)]);
                if rand<0.5
                    newreg.Xconj = [newreg.Xconj,self.X(selected)];
                else
                    newreg.Xenv = [newreg.Xenv,self.X(selected)];
                end
                newreg.X(selected) = [];
            elseif ind <= length(self.Xconj)+length(self.X)
                selected = randi([1,length(self.Xconj)]);
                if rand<0.5
                    newreg.X = [newreg.X,self.Xconj(selected)];
                else
                    newreg.Xenv = [newreg.Xenv,self.Xconj(selected)];
                end
                newreg.Xconj(selected) = [];
            else
                selected = randi([1,length(self.Xenv)]);
                if rand<0.5
                    newreg.X = [newreg.X,self.Xenv(selected)];
                else
                    newreg.Xconj = [newreg.Xconj,self.Xenv(selected)];
                end
                newreg.Xenv(selected) = [];
            end
        end
    %%  mutateorder2:
        function newreg = mutateorder2(self)
            newreg = Regressor(self.X,self.Xconj,self.Xenv);
            if rand<0.5
                newreg.X = [newreg.X 0];
            else
                newreg.Xconj = [newreg.Xconj 0];
            end
        end
    %%  mutateorder2v2: 
%         function self = mutateorder2v2(self)
%             ind = randi([1 length(self.X)+length(self.Xconj)+length(self.Xenv)]);
%             if ind <= length(self.X)
%                 self.X = self.X(ind) + 1;
%             elseif ind <= length(self.Xconj)
%                 ind = ind-length(self.X);
%                 self.Xconj(ind) = self.Xconj(ind) + 1;
%             else
%                 ind = ind - length(self.X) - length(self.Xconj);
%                 self.Xenv(ind) = self.Xenv(ind) + 1;
%             end
%         end
    %%  fullCrossover: 
    %       The target object merges with another one, concatenating
    %       its parameters, obtaining a child with the sum of all the 
    %       info.
        function newreg = fullCrossover(self, parent)
            Xn = [self.X parent.X];
            Xconjn = [self.Xconj parent.Xconj];
            Xenvn = [self.Xenv parent.Xenv];
            newreg = Regressor(Xn,Xconjn,Xenvn);
        end
    %%  crossover: 
    %       The same as fullcrossover but only half the genetic info
    %       is preserved.
        function newreg = crossover(self, parent)
            ph = ([self.X parent.X]);
            ph = ph(randperm(length(ph)));
            Xn = ph(1:floor((length(self.X)+length(parent.X))/2));
            ph = ([self.Xconj parent.Xconj]);
            ph = ph(randperm(length(ph)));
            Xconjn = ph(1:floor((length(self.Xconj)+length(parent.Xconj))/2));
            ph = ([self.Xenv parent.Xenv]);
            ph = ph(randperm(length(ph)));
            Xenvn = ph(1:floor((length(self.Xenv)+length(parent.Xenv))/2));
            newreg = Regressor(Xn,Xconjn,Xenvn);
        end
    %%   buildRegressor: creates the regressor
        function self = buildRegressor(self, x, n, Qpmax, Qnmax)
%             self.print_mig()
            
            y = ones(size(n(1+Qpmax:end-Qnmax)));
            
            for i=1:length(self.X)
                y = y.*x(n(1+Qpmax-self.X(i):end-Qnmax-self.X(i)));
            end
            for i=1:length(self.Xconj)
                y = y.*conj(x(n(1+Qpmax-self.Xconj(i):end-Qnmax-self.Xconj(i))));
            end
            for i=1:length(self.Xenv)
                y = y.*abs(x(n(1+Qpmax-self.Xenv(i):end-Qnmax-self.Xenv(i))));
            end
            
            self.reg = y;
        end
     %% order: orders parameter
        function self = order(self)
            if ~isempty(self.X)
            self.X = sort(self.X);
            end
            if ~isempty(self.Xconj)
            self.Xconj = sort(self.Xconj);
            end
            if ~isempty(self.Xenv)
            self.Xenv = sort(self.Xenv);
            end
        end
     %% getEnv: computes Xenv terms
     %      Transforms all X,Xconj values to Xenv is possible following
     %      the X*Xconj = Xenv^2 eq.
        function self = getEnv(self)
            for i=unique(self.X)
                occ = sum(i==self.Xconj);
                occ2 = sum(i == self.X);
                occ = min([occ,occ2]);
                if occ
                    self.Xenv = [self.Xenv i*ones(1,2*occ)];
                    self.X = self.X(setdiff(1:length(self.X),find(i==self.X,occ)));
                    self.Xconj = self.Xconj(setdiff(1:length(self.Xconj),find(i==self.Xconj,occ)));
                end
            end
        end
%      %% computeScore: Deprecated
%         function self = computeScore(self,x,y,n,Qpmax,Qnmax)
%             self.buildRegressor(x,n,Qpmax,Qnmax);
%             % TO DO, correlación r con y, calcula score pero seguramente
%             % haya formas bastante más eficientes y/o que den mejores
%             % resultados.
%             self.score = abs(sum(y.*self.reg));
%         end
    end
end