function [rManager] = GVGgenerateModel(u,y,generations,n,Qpmax,Qnmax)
    nmse = @(ymod,y) 20*log10(norm(ymod-y)/norm(y));
    dBm = @(x) 10*log10(rms(x).^2/100)+30;

    %% GA exploring regressor space
    rManager = regressorManager(200,u,y,n,Qpmax,Qnmax);
    rManager.populateJA();
    rManager.DOMPizePopulation(y,0);
    
    values = 1:-0.25/50:0.5;
    mutateRate = linspace(0.3,0.7,100);
    mutateRate = [mutateRate linspace(0.7,0.7,100)];
    nmses = [];
    nmsesf = [];
    popSize = [];
    hardPurge = 0;
    tiemposGA = [];
    for gen = 1:generations
        tic
        if gen > 50
            g = 0.5;
        else
            g = values(gen);
        end
    
        rManager.selectionNewGen(0.5);
        if gen > 5
    %         if hardPurge
    %             rManager.mutatePopulation(1);
    %         else
    %             rManager.mutatePopulation(mutateRate(gen));
    %         end
            rManager.mutatePopulation(mutateRate(gen));
        end
        if gen > 1
            rManager.purge(hardPurge);
        end
        rManager.DOMPizePopulation(y,0);
        [ymod,ymodf] = rManager.testModel(y);
        nmses = [nmses, nmse(ymod,y)];
        nmsesf = [nmsesf, nmse(ymodf,y)];
        popSize = [popSize, length(rManager.regPopulation)];
    
    
%         close all
       
        fprintf('--------------------- Generation %d --------------------------',gen)
        fprintf(newline)
        tiemposGA = [tiemposGA toc];
    end
 
    figure(1)
    plot(1:gen,popSize)
    saveas(gcf,'../../results/popSize.jpg');
    figure(2)
    scatter(popSize,nmses)
    figure(3)
    plot(1:gen,nmses,'o')
    hold on
    plot(1:gen,nmsesf,'o')
    plot(1:gen,rManager.nmseDOMP(2:end),'o')
    legend('Nopt','full','DOMP')
    annotation('textbox', [0.6, 0.6, 0.1, 0.1], 'String', ['NMSE id: ',num2str(nmsesf(end))])
    drawnow() 
    saveas(gcf,'../../results/nmseVsgenerations.jpg');

    U = rManager.U;
end