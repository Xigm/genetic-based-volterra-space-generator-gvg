classdef regressorManager < handle
    properties
        regPopulation
        maxPopulation
        x
        y
        n
        Qpmax
        Qnmax
        U
        s
        nopt
        nmseDOMP
    end
    %%
    methods
    %% regressorManager: builder
        function self = regressorManager(maxPop,x,y,n,Qpmax,Qnmax)
            self.maxPopulation = maxPop;
            self.x = x;
            self.y = y;
            self.n = n;
            self.Qpmax = Qpmax;
            self.Qnmax = Qnmax;
            self.nmseDOMP = [];
            self.regPopulation = [];
        end
    %% populateJA: creates init population
    %       Create the population with a defined initial set:
    %       3 regressors: R([0],[],[]),R([],[0],[]),R([],[],[0])
        function self = populateJA(self)
            self.regPopulation = [self.regPopulation Regressor([0],[],[])]; %#ok<*NBRAK> 
            self.regPopulation = [self.regPopulation Regressor([],[0],[])];
            self.regPopulation = [self.regPopulation Regressor([],[],[0])];
        end
        
    %% mutatePopulation: Mutates part of the populaton,
    %       Mutates part of the populaton,
    %       determinated by the input P [0,1].  
    %       It is assumed that population is already sorted.
        function self = mutatePopulation(self,P)
            Xmutate = self.regPopulation(1:floor(length(self.regPopulation)*P));
            for i = 1:length(Xmutate)
                r = rand;
                if r < 1/3
                    self.regPopulation = [self.regPopulation Xmutate(i).mutateorderv2()];
                elseif r < 2/3
                    self.regPopulation = [self.regPopulation Xmutate(i).mutatememory()];
                else
                    self.regPopulation = [self.regPopulation Xmutate(i).mutateorder2()];
                end
            end
        end
    %% orderByScore: 
    %       Orders by the fitness function value.
        function self = orderByScore(self)
            scores = [];
            indexes = [];
            cont = 1;
            for reg = self.regPopulation
                reg.computeScore(self.x,self.y,self.n,self.Qpmax,self.Qnmax);
                scores = [scores reg.score]; %#ok<AGROW> 
                indexes = [indexes cont]; %#ok<AGROW> 
                cont = cont + 1;
            end
            [~,indexes] = sort(scores,'ascend');
            self.regPopulation = self.regPopulation(indexes);
        end
    %% newGen: creates the next generation
    %       Mixes the best regressors between them to create
    %       the next generation of regressors. 
        function self = newGen(self,num)
            if num > length(self.regPopulation)
                num = length(self.regPopulation);
            end
            rp = randperm(num);
            for i = 1:num
                r = self.regPopulation(i);
                newr = r.fullCrossover(self.regPopulation(rp(i)));
                self.regPopulation = [self.regPopulation newr];
            end
        end
    %% performSelection: 
    %       It will be deleted this percentage (1-perc)*100%
        function self = performSelection(self,P)
            if nargin == 2
                self.regPopulation = self.regPopulation(1:floor(length(self.regPopulation)*P));
            else
                self.regPopulation = self.regPopulation(1:self.maxPopulation);
            end
        end

    %% performHardSelection: 
    %   Deprecated
        function self = performHardSelection(self)
            j = 1;
            while j <= length(self.regPopulation)
                if isnan(self.regPopulation(j).score)
                    self.regPopulation(j) = [];
                    j = j-1;
                end
                j = j+1;
            end
        end
    %% buildU
    %   Builds the full regressor matrix
        function self = buildU(self)
            U = [];
            N = length(self.y);
            for i = 1:length(self.regPopulation)
                self.regPopulation(i).buildRegressor(self.x, [N-self.Qpmax+1:N , 1:N , 1:self.Qnmax].',self.Qpmax,self.Qnmax);
                U =  [U,self.regPopulation(i).reg];
            end
%             self.U = U(self.Qpmax+1:end-self.Qnmax,:);
            self.U = U;
        end
    %% buildU
    %   Builds a reduced version of U, using indexes n
        function redU = buildReducedU(self)
            U = [];
            for i = 1:length(self.regPopulation)
                self.regPopulation(i).buildRegressor(self.x, self.n,self.Qpmax,self.Qnmax);
                U =  [U,self.regPopulation(i).reg];
            end
%             self.U = U(self.Qpmax+1:end-self.Qnmax,:);
            redU = U;
        end
    %% purge: deletes repeated regs
        function self = purge(self,hardPurge)
            for i = 1:length(self.regPopulation)
                self.regPopulation(i).getEnv();
                self.regPopulation(i).order();
            end
            purgedRep = 0;
            purgedScore = 0;
            for i = 1:length(self.regPopulation)
                j = i+1;
                while j <= length(self.regPopulation)
                    if self.regPopulation(i).equals(self.regPopulation(j)) 
                        self.regPopulation(j) = [];
                        j = j-1;
                        purgedRep = purgedRep +1;
                    elseif (hardPurge == 1)
                        if (self.regPopulation(i).score > 2)
                            self.regPopulation(j) = [];
                            j = j-1;
                            purgedScore = purgedScore +1;
                        end
                    end
                    j = j+1;
                end
            end
            fprintf('purged because of repeated %d | purged because of bad score %d ',purgedRep,purgedScore);
            fprintf(newline)
        end

    %% selectionNewgen: performSelection + newGen
        function self = selectionNewGen(self,renewRate)
            if length(self.regPopulation) < self.maxPopulation/1.5
                self.newGen(length(self.regPopulation));
            elseif length(self.regPopulation) < self.maxPopulation
                self.performSelection(renewRate);
                self.newGen(renewRate*self.maxPopulation);
            else
                self.performSelection(renewRate);
                self.newGen(0.5*renewRate*self.maxPopulation);
            end
        end
    %% testModel: checks actual model of regs
        function [y_mod,y_modf] = testModel(self,y)
           if length(y) == length(self.n)
               self.buildU();
               normVecNopt = vecnorm(self.U(self.n,self.s(1:self.nopt)));
               Unopt = self.U(self.n,self.s(1:self.nopt))*diag(normVecNopt.^(-1));
               normVec = vecnorm(self.U(self.n,:));
               Un = self.U(self.n,:)*diag(normVec.^(-1));
               y_mod = Unopt*(pinv(Unopt)*y);
               y_modf = Un*(pinv(Un)*y);
           else
               self.buildU();
               normVecNopt = vecnorm(self.U(:,self.s(1:self.nopt)));
               Unopt = self.U(:,self.s(1:self.nopt))*diag(normVecNopt.^(-1));
               normVec = vecnorm(self.U);
               Un = self.U*diag(normVec.^(-1));
               y_mod = Unopt*(pinv(Unopt)*y);
               y_modf = Un*(pinv(Un)*y);
           end
        end
    %% getNMSES
        function [nmseIdNopt,nmseIdFull,nmseValNopt,nmseValFull] = getNMSES(self,y)
           nmse = @(ymod,y) 20*log10(norm(ymod-y)/norm(y));
           [y_mod,y_modf] = testModel(self,y(self.n));
           nmseIdNopt = nmse(y_mod,y(self.n));
           nmseIdFull = nmse(y_modf,y(self.n));
           [y_mod,y_modf] = testModel(self,y);
           nmseValNopt = nmse(y_mod,y);
           nmseValFull = nmse(y_modf,y);
        end
    %% zipMe
    %   Stores important information and deletes the rest to be saved
        function self = zipMe(self)
            for reg = self.regPopulation
                reg.reg = [];
            end
            self.x = [];
            self.y = [];
            self.n = [];
            self.U = [];
            self.nmseDOMP = [];
        end
    %% shakeIt: 
    %   Changes randomly the order of the population
        function self = shakeIt(self)
            self.regPopulation = [self.regPopulation(1), self.regPopulation(randperm(length(self.regPopulation)-1)+1)];
        end
    %% DOMPizePopulation
    %   Executes domp over the population
        function self = DOMPizePopulation(self,y,verbose)
%             self.shakeIt();
            self.buildU();
            y = y(self.n);
            [~,N] = size(self.U);
            n2 = self.n;
            % Normalization of input matrix
            normVec = vecnorm(self.U(n2,:));
            Un = self.U(n2,:)*diag(normVec.^(-1));

            Ncoef = length(self.regPopulation);
            
            % Initialization
            Zn = Un;
            h = zeros(N, 1);    
            ve2 = zeros(1, min(Ncoef,N));
            r = y;
            yest = zeros(size(y));
            s = []; % Support set is empty
            Tacc = eye(N);
            RZ = Zn'*Zn;
            Ry = Zn'*y;

            for k = 1:min(Ncoef,N) 
    
                % B. Regressor selection
                [caux,saux]=sort(abs(Ry./sqrt(diag(RZ))),'descend');
                aux2 = setxor(saux,s,'stable'); % xor removes the elements of saux in s
                s(k) = aux2(1);
                
                % Construction of T
                T = eye(N);
                T(s(k), setxor(s,1:N)) = -RZ(s(k),setxor(s,1:N))/(RZ(s(k),s(k)));
                T(s(k), s(k)) = 1/sqrt(RZ(s(k),s(k)));
                
                % D. Residual correlation update
                Ry(setxor(s,1:N)) = Ry(setxor(s,1:N)) - (RZ(s(k),setxor(s,1:N))/RZ(s(k),s(k)))' * Ry(s(k));
                Ry(s(k)) = Ry(s(k))/sqrt(RZ(s(k),s(k)));
                
                % E. Autocorrelation update
                RZ = T'*RZ*T;
                
                % C. Orthogonalization
                Zn = Zn*T;
                r = r - Zn(:,s(k))*Ry(s(k));
                ve2(k) = var(y-yest);
                nmse(k)=20*log10(norm(r,2)/norm(y,2));
                
                % The estimations are updated according to Tacc every X iterations to
                % avoid the accumulation of numerical errors
                if(rem(k,50)==0)
                    RZ = Zn'*Zn;
                    Ry = Zn'*y;
                end
                
                % F. Regression
                % Here we expect to have h in the Volterra space unnormalized (the
                % paper expects to have it normalized):
                h(s,k) = diag(normVec(s).^-1)*Tacc(s,s)*Ry(s);
                
                % Estimation update
                yest = yest + Zn(:,s(k))*Ry(s(k));
                
                % Accumulation of T
                Tacc = Tacc*T;
                % Plot the correlation matrices in iterations 1, 5 and 20
                
                % Print the iteration, the index of the selected regressor, the text
                % representation of the regressor and the attained NMSE.
                fprintf('%d | %d | %s | %4.1f\n',k,s(k),self.regPopulation(k).print_mig(),nmse(k));
               
            end
            M = length(y);
            BIC = 2*M*log(ve2)+2*(1:min(Ncoef,N))*log(2*M); % complex
            [~, self.nopt] = min(BIC);

            if verbose
%                 nmses(k)=20*log10(norm(r,2)/norm(y,2));
                % Plots of the NMSE and BIC evolution with the number of iterations
                figure;clf;plot(nmse,'b','linewidth',2);hold on;plot(self.nopt,nmse(self.nopt),'ro');legend('NMSE','Optimum');xlabel('Number of coefficients'); ylabel('NMSE (dB)')
            end

            self.s = s;
            self.regPopulation = self.regPopulation(s);
            for i = 1:length(self.regPopulation)
                if isnan(nmse(i))
                    self.regPopulation(i).score = 0;
                else
                    if i == 1
                        self.regPopulation(i).score = nmse(i);
                    else
                        self.regPopulation(i).score = nmse(i)-nmse(i-1);
                    end
                end
            end
            self.nmseDOMP = [self.nmseDOMP,min(nmse)];
        end
    %% ValDOMPizePopulation
    %   Dompizes with the full waveform, not using indexes
        function self = ValDOMPizePopulation(self,y,verbose)
%             self.shakeIt();
            self.buildU();
%             y = y(self.n);
            [~,N] = size(self.U);
%             n2 = self.n;
            % Normalization of input matrix
            normVec = vecnorm(self.U);
            Un = self.U*diag(normVec.^(-1));

            Ncoef = length(self.regPopulation);
            
            % Initialization
            Zn = Un;
            h = zeros(N, 1);    
            ve2 = zeros(1, min(Ncoef,N));
            r = y;
            yest = zeros(size(y));
            s = []; % Support set is empty
            Tacc = eye(N);
            RZ = Zn'*Zn;
            Ry = Zn'*y;

            for k = 1:min(Ncoef,N) 
    
                % B. Regressor selection
                [caux,saux]=sort(abs(Ry./sqrt(diag(RZ))),'descend');
                aux2 = setxor(saux,s,'stable'); % xor removes the elements of saux in s
                s(k) = aux2(1);
                
                % Construction of T
                T = eye(N);
                T(s(k), setxor(s,1:N)) = -RZ(s(k),setxor(s,1:N))/(RZ(s(k),s(k)));
                T(s(k), s(k)) = 1/sqrt(RZ(s(k),s(k)));
                
                % D. Residual correlation update
                Ry(setxor(s,1:N)) = Ry(setxor(s,1:N)) - (RZ(s(k),setxor(s,1:N))/RZ(s(k),s(k)))' * Ry(s(k));
                Ry(s(k)) = Ry(s(k))/sqrt(RZ(s(k),s(k)));
                
                % E. Autocorrelation update
                RZ = T'*RZ*T;
                
                % C. Orthogonalization
                Zn = Zn*T;
                r = r - Zn(:,s(k))*Ry(s(k));
                ve2(k) = var(y-yest);
                nmse(k)=20*log10(norm(r,2)/norm(y,2));
                
                % The estimations are updated according to Tacc every X iterations to
                % avoid the accumulation of numerical errors
                if(rem(k,50)==0)
                    RZ = Zn'*Zn;
                    Ry = Zn'*y;
                end
                
                % F. Regression
                % Here we expect to have h in the Volterra space unnormalized (the
                % paper expects to have it normalized):
                h(s,k) = diag(normVec(s).^-1)*Tacc(s,s)*Ry(s);
                
                % Estimation update
                yest = yest + Zn(:,s(k))*Ry(s(k));
                
                % Accumulation of T
                Tacc = Tacc*T;
                % Plot the correlation matrices in iterations 1, 5 and 20
                
                % Print the iteration, the index of the selected regressor, the text
                % representation of the regressor and the attained NMSE.
                fprintf('%d | %d | %s | %4.1f\n',k,s(k),self.regPopulation(k).print_mig(),nmse(k));
               
            end
            M = length(y);
            BIC = 2*M*log(ve2)+2*(1:min(Ncoef,N))*log(2*M); % complex
            [~, self.nopt] = min(BIC);

            if verbose
%                 nmses(k)=20*log10(norm(r,2)/norm(y,2));
                % Plots of the NMSE and BIC evolution with the number of iterations
                figure;clf;plot(nmse,'b','linewidth',2);hold on;plot(self.nopt,nmse(self.nopt),'ro');legend('NMSE','Optimum');xlabel('Number of coefficients'); ylabel('NMSE (dB)')
            end

            self.s = s;
            self.regPopulation = self.regPopulation(s);
            for i = 1:length(self.regPopulation)
                if isnan(nmse(i))
                    self.regPopulation(i).score = 0;
                else
                    if i == 1
                        self.regPopulation(i).score = nmse(i);
                    else
                        self.regPopulation(i).score = nmse(i)-nmse(i-1);
                    end
                end
            end
            self.nmseDOMP = [self.nmseDOMP,min(nmse)];
        end
    end
end