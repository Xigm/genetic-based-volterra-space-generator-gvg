clearvars;
clc;
close all;

% set seed and load scripts for the GVG
rng(1004);
addpath('GVG');

% load signal data and save vars for validation at the end
load('../data/measChalmers.mat','u','y');
u0 = u;
y0 = y;

% uncomment these lines to use the full version of the algorithm
% if not, only a 0.05^2 portion of the signal is used to identificate
n = sel_indices(u,y,0.05).';
u = u(n);
y = y(n);

% set some parameters for the algorithm
N = length(u);
Qpmax = 30;
Qnmax = 30;
% n =  [N-Qpmax+1:N , 1:N , 1:Qnmax].';
n = sel_indices(u,y,0.05).';
generations = 100;

%% GA exploring regressor space
[rManager] = GVGgenerateModel(u,y,generations,n,Qpmax,Qnmax);

% validation of results achieved
rManager.x = u0;
rManager.y = y0;
rManager.ValDOMPizePopulation(y0,1)
saveas(gcf,'../results/DOMPfinalmodel.png');

